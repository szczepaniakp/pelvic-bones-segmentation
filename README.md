# Comparison of two- and three-dimensional deep networks for the segmentation of bone structures

## About
This project compares UNet in two versions - 2D and 3D. It uses medical images (CTs) of pelvic bone structures to segment it from its background.

--- 

## Using notebooks
- Make sure your machine have (NVIDIA) GPU. 
- Generate dataset using instructions below.
- Create directories named ```3D_results``` and ```2D_results``` in the same folder where jupyter notebooks are placed. All generated images and plots for each model will appear here.

---
## TFDS - generating data

1. Download two files from https://zenodo.org/record/4588403#.Y54tDi_URcf (description of data are placed in https://github.com/ICT-MIRACLE-lab/CTPelvic1K) and unzip them to specific folder:
    * CTPelvic1K_dataset6_data.tar.gz -> ```./data/images```, 
    * CTPelvic1K_dataset6_Anonymized_mask.tar.gz -> ```./data/masks```.

2. To generate data used to train networks follow instructions on https://www.tensorflow.org/datasets/add_dataset and create *locally* custom Tensorflow Dataset. Use names ```clinic_data_2d``` and/or ```clinic_data``` (for 3D UNet). 

    _Note: You can precise in which folder config files are to be saved. For convinience use ```./data```._

3. Next, swap config file (```clinic_data.py```/```clinic_data_2d.py```) from folder ```tensorflow_datasets_config_files``` with the one in folder generated in previous step.

4. Edit swapped file and change variable (```extracted_path```) in line 79/80 to the path of a parent directory of your unzipped data (e.g. ```./data```).

5. Determine environmental variable ```TFDS_DATA_DIR``` to ```./data/tensorflow_datasets``` or leave it as it is. 

6. Enter generated folder and run:
> tfds build 

7. Modify TFDS paths in jupyter notebooks to match your local files structure.