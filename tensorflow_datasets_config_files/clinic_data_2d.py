"""clinic_data_2d dataset."""
import glob
import tensorflow_datasets as tfds
import tensorflow as tf
import skimage.transform as transform
import numpy as np
import nibabel as nib
import zipfile
import os


_DESCRIPTION = """
"""

_CITATION = """
"""

SLICES_NUM = 288 # max slices number
IMG_SIZE = (128,128) 


class ClinicData2d(tfds.core.GeneratorBasedBuilder):
  """DatasetBuilder for clinic_data dataset."""

  VERSION = tfds.core.Version('1.0.2')
  RELEASE_NOTES = {
      '1.0.1': 'normalized, 128x128, float32',
      '1.0.2': 'disable shuffling',
  }
  
  def add_gaussian_noise(self, inp, expected_noise_ratio=0.05):
    image = inp.copy()
    if len(image.shape) == 2:
        row,col= image.shape
        sl = 1
    else:
        row,col,sl= image.shape
    mean = 0.
    var = 0.1
    sigma = var**0.5
    gauss = np.random.normal(mean,sigma,(row,col,sl)) * expected_noise_ratio
    gauss = gauss.reshape(row,col,sl)
    noisy = image + gauss
    return noisy
    
  def normalize(self, img):
    arr = img.copy().astype(np.float32)
    m = np.float32(np.min(img))
    M = np.float32(np.max(img))
    
    arr -= m
    
    if M != 0:
        arr *= 1./(M-m)
        
    return arr
        
  def _info(self) -> tfds.core.DatasetInfo:
    """Returns the dataset metadata."""
    
    return tfds.core.DatasetInfo(
        builder=self,
        description=_DESCRIPTION,
        features=tfds.features.FeaturesDict({
            'scan': tfds.features.Sequence({
                'image': tfds.features.Image(shape=(*IMG_SIZE, 1), dtype=tf.float32),
                'mask': tfds.features.Image(shape=(*IMG_SIZE, 1), dtype=tf.uint8),
            }),
        }),
        
        supervised_keys=None, 
        homepage='https://zenodo.org/record/4588403#.YEyLq_0zaCo',
        citation=_CITATION,
        disable_shuffling=True
    )
    
  def _split_generators(self, dl_manager: tfds.download.DownloadManager):
    """Returns SplitGenerators."""

    extracted_path = tfds.core.Path(
        '/nfs-home/Szczepaniak_Patrycja_Pregowska_Anna/data'
    )

    return [
      tfds.core.SplitGenerator(
         name=tfds.Split.TRAIN,
         gen_kwargs={
             "image_path": extracted_path / 'images',
             "mask_path": extracted_path / 'masks'
         },
      )
    ]
     
  def _generate_examples(self, image_path=None, mask_path=None):
    """Yields examples."""
    
    list_images = sorted(filter(os.path.isfile,
                            image_path.glob('*.nii.gz')))
    
    list_masks = sorted(filter(os.path.isfile,
                            mask_path.glob('*.nii.gz')))
    n = 0
    
    for img_p, mask_p in zip(list_images, list_masks):
      
      img = nib.load(image_path / img_p)
      mask = nib.load(mask_path / mask_p)
    
      # img
      org_slices_num = img.dataobj.shape[2]
      new_shape = (*IMG_SIZE, org_slices_num)
      
      resized_img = transform.resize(
        np.array(img.dataobj),
        new_shape,
        order=0,
        preserve_range=True,
        anti_aliasing=False
      ).astype('int16')
      
      norm_img = self.normalize(resized_img)
      gauss_img = self.add_gaussian_noise(norm_img)
        
      slices1 = gauss_img.transpose() 
      
      # mask
      resized_mask = transform.resize(
        np.asanyarray(mask.dataobj, dtype=np.uint8),
        new_shape,
        order=0,
        preserve_range=True,
        anti_aliasing=False
      ).astype('uint8')
      
      mapped_mask = resized_mask > 0
      resized_mask[mapped_mask] = 1
      
      slices2 = resized_mask.transpose()
      n = n + 1
      
      # iterate over slices
      lower_thresh = int((org_slices_num - SLICES_NUM)/2)
      upper_thresh = lower_thresh + SLICES_NUM
      
      seq_images = []
      seq_masks = []
      
      for i in range(lower_thresh, upper_thresh, 9):

        image = slices1[i]
        image = image[..., None]
        mask = slices2[i]
        mask = mask[..., None]
        
        name = f'patient_{n}_{i}'
       
        imageTensor = np.array(image, dtype=np.float32)
        maskTensor = np.array(mask, dtype=np.uint8)
        
        scan = {
            'image': [imageTensor],
            'mask': [maskTensor]
        }
        
        yield name, {
            'scan': scan,
        }
      
