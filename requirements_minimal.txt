imagehash==4.3.1
ipywidgets==8.0.2
nibabel==4.0.1
pandas-profiling==3.3.0
pydicom==2.3.0
scikit-image==0.19.3
tensorflow-gpu==2.10.0
tfds-nightly==4.7.0.dev202210060045
# absl-py==1.2.0 # Installed as dependency for tensorflow-metadata, tfds-nightly, tensorflow-gpu, tensorboard
# asttokens==2.0.8 # Installed as dependency for stack-data
# astunparse==1.6.3 # Installed as dependency for tensorflow-gpu
# attrs==22.1.0 # Installed as dependency for visions
# backcall==0.2.0 # Installed as dependency for ipython
# cachetools==5.2.0 # Installed as dependency for google-auth
# certifi==2022.9.24 # Installed as dependency for requests
# charset-normalizer==2.1.1 # Installed as dependency for requests
# cycler==0.11.0 # Installed as dependency for matplotlib
# debugpy==1.6.3 # Installed as dependency for ipykernel
# decorator==5.1.1 # Installed as dependency for ipython
# dill==0.3.5.1 # Installed as dependency for tfds-nightly
# entrypoints==0.4 # Installed as dependency for jupyter-client
# etils==0.8.0 # Installed as dependency for tfds-nightly
# executing==1.1.0 # Installed as dependency for stack-data
# flatbuffers==22.9.24 # Installed as dependency for tensorflow-gpu
# gast==0.4.0 # Installed as dependency for tensorflow-gpu
# google-auth==2.12.0 # Installed as dependency for tensorboard, google-auth-oauthlib
# google-auth-oauthlib==0.4.6 # Installed as dependency for tensorboard
# google-pasta==0.2.0 # Installed as dependency for tensorflow-gpu
# googleapis-common-protos==1.56.4 # Installed as dependency for tensorflow-metadata
# grpcio==1.49.1 # Installed as dependency for tensorboard, tensorflow-gpu
# h5py==3.7.0 # Installed as dependency for tensorflow-gpu
# htmlmin==0.1.12 # Installed as dependency for pandas-profiling
# idna==3.4 # Installed as dependency for requests
# imageio==2.19.3 # Installed as dependency for scikit-image
# importlib-metadata==5.0.0 # Installed as dependency for markdown
# importlib-resources==5.9.0 # Installed as dependency for tfds-nightly
# ipykernel==6.16.0 # Installed as dependency for ipywidgets
# ipython==8.5.0 # Installed as dependency for ipykernel, ipywidgets
# jedi==0.18.1 # Installed as dependency for ipython
# jinja2==3.1.2 # Installed as dependency for pandas-profiling
# joblib==1.1.0 # Installed as dependency for pandas-profiling, phik
# jupyter-client==7.3.5 # Installed as dependency for ipykernel
# jupyter-core==4.11.1 # Installed as dependency for jupyter-client
# jupyterlab-widgets==3.0.3 # Installed as dependency for ipywidgets
# keras==2.10.0 # Installed as dependency for tensorflow-gpu
# keras-preprocessing==1.1.2 # Installed as dependency for tensorflow-gpu
# kiwisolver==1.4.4 # Installed as dependency for matplotlib
# libclang==14.0.6 # Installed as dependency for tensorflow-gpu
# markdown==3.4.1 # Installed as dependency for tensorboard
# markupsafe==2.1.1 # Installed as dependency for jinja2, werkzeug
# matplotlib==3.2.1 # Installed as dependency for pandas-profiling, missingno, phik, seaborn
# matplotlib-inline==0.1.3 # Installed as dependency for ipykernel, ipython
# missingno==0.5.1 # Installed as dependency for pandas-profiling
# multimethod==1.8 # Installed as dependency for pandas-profiling, visions
# nest-asyncio==1.5.6 # Installed as dependency for jupyter-client, ipykernel
# networkx==2.8.7 # Installed as dependency for visions, scikit-image
# numpy==1.23.0 # Installed as dependency for pywavelets, matplotlib, scipy, keras-preprocessing, seaborn, tensorboard, nibabel, opt-einsum, phik, missingno, imagehash, visions, tfds-nightly, tensorflow-gpu, scikit-image, imageio, statsmodels, h5py, tifffile, patsy, pandas, pandas-profiling
# oauthlib==3.2.1 # Installed as dependency for requests-oauthlib
# opt-einsum==3.3.0 # Installed as dependency for tensorflow-gpu
# packaging==21.3 # Installed as dependency for ipykernel, tensorflow-gpu, statsmodels, nibabel, scikit-image
# pandas==1.4.3 # Installed as dependency for visions, statsmodels, phik, pandas-profiling, seaborn
# parso==0.8.3 # Installed as dependency for jedi
# patsy==0.5.2 # Installed as dependency for statsmodels
# pexpect==4.8.0 # Installed as dependency for ipython
# phik==0.12.2 # Installed as dependency for pandas-profiling
# pickleshare==0.7.5 # Installed as dependency for ipython
# pillow==9.2.0 # Installed as dependency for scikit-image, imageio, imagehash
# promise==2.3 # Installed as dependency for tfds-nightly
# prompt-toolkit==3.0.31 # Installed as dependency for ipython
# protobuf==3.19.0 # Installed as dependency for tfds-nightly, tensorflow-gpu, tensorboard, googleapis-common-protos, tensorflow-metadata
# psutil==5.9.2 # Installed as dependency for ipykernel
# ptyprocess==0.7.0 # Installed as dependency for pexpect
# pure-eval==0.2.2 # Installed as dependency for stack-data
# pyasn1==0.4.8 # Installed as dependency for pyasn1-modules, rsa
# pyasn1-modules==0.2.8 # Installed as dependency for google-auth
# pydantic==1.9.2 # Installed as dependency for pandas-profiling
# pygments==2.13.0 # Installed as dependency for ipython
# pyparsing==3.0.9 # Installed as dependency for packaging, matplotlib
# python-dateutil==2.8.2 # Installed as dependency for jupyter-client, matplotlib, pandas
# pytz==2022.4 # Installed as dependency for pandas
# pywavelets==1.4.1 # Installed as dependency for scikit-image, imagehash
# pyyaml==6.0 # Installed as dependency for pandas-profiling
# pyzmq==24.0.1 # Installed as dependency for jupyter-client, ipykernel
# requests==2.28.1 # Installed as dependency for pandas-profiling, requests-oauthlib, tfds-nightly, tensorboard
# requests-oauthlib==1.3.1 # Installed as dependency for google-auth-oauthlib
# rsa==4.9 # Installed as dependency for google-auth
# scipy==1.8.1 # Installed as dependency for statsmodels, imagehash, scikit-image, phik, pandas-profiling, missingno, seaborn
# seaborn==0.11.2 # Installed as dependency for pandas-profiling, missingno
# six==1.16.0 # Installed as dependency for tfds-nightly, tensorflow-gpu, promise, grpcio, astunparse, python-dateutil, google-auth, google-pasta, patsy, asttokens, keras-preprocessing
# stack-data==0.5.1 # Installed as dependency for ipython
# statsmodels==0.13.2 # Installed as dependency for pandas-profiling
# tangled-up-in-unicode==0.2.0 # Installed as dependency for pandas-profiling, visions
# tensorboard==2.10.1 # Installed as dependency for tensorflow-gpu
# tensorboard-data-server==0.6.1 # Installed as dependency for tensorboard
# tensorboard-plugin-wit==1.8.1 # Installed as dependency for tensorboard
# tensorflow-estimator==2.10.0 # Installed as dependency for tensorflow-gpu
# tensorflow-io-gcs-filesystem==0.27.0 # Installed as dependency for tensorflow-gpu
# tensorflow-metadata==1.10.0 # Installed as dependency for tfds-nightly
# termcolor==2.0.1 # Installed as dependency for tfds-nightly, tensorflow-gpu
# tifffile==2022.8.12 # Installed as dependency for scikit-image
# toml==0.10.2 # Installed as dependency for tfds-nightly
# tornado==6.2 # Installed as dependency for jupyter-client, ipykernel
# tqdm==4.64.1 # Installed as dependency for pandas-profiling, tfds-nightly
# traitlets==5.4.0 # Installed as dependency for jupyter-core, ipykernel, ipywidgets, ipython, jupyter-client, matplotlib-inline
# typing-extensions==4.3.0 # Installed as dependency for pydantic, tensorflow-gpu
# urllib3==1.26.12 # Installed as dependency for requests
# visions==0.7.5 # Installed as dependency for pandas-profiling
# wcwidth==0.2.5 # Installed as dependency for prompt-toolkit
# werkzeug==2.2.2 # Installed as dependency for tensorboard
# widgetsnbextension==4.0.3 # Installed as dependency for ipywidgets
# wrapt==1.14.1 # Installed as dependency for tensorflow-gpu
# zipp==3.8.1 # Installed as dependency for importlib-resources, importlib-metadata
